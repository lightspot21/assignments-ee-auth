#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int **Dyn2dArray(int rows, int cols)
{
	int **ptr = calloc(rows, sizeof(int*));
	for (int i = 0; i < rows; i++)
	{
		*(ptr + i) = calloc(cols, sizeof(int));
	}
	return ptr;
}

int printArray(int **arr, int rows, int cols)
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d  ", arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}

int *LineSum(char mode, int **arr, int rows, int cols)
{
	switch (mode)
	{
		int *sum;
		case 'r':
		    sum = calloc(rows, sizeof(int));
		    for (int i = 0; i < rows; i++)
		    {
		    	for (int j = 0; j < cols; j++)
		    	{
		    		sum[i] += arr[i][j];
		    	}
		    }
		    return sum;
		break;
		case 'c':
		    sum = calloc(cols, sizeof(int));
		    for (int i = 0; i < cols; i++)
		    {
		    	for (int j = 0; j < rows; j++)
		    	{
		    		sum[i] += arr[j][i];
		    	}
		    }
		    return sum;
		break;
	}
}

int magicSquare(int **arr, int rows, int cols)
{
	srand(time(NULL));
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols - 1; j++)
		{
			arr[i][j] = rand() % 100;
		}
	}
	int *rowsum = LineSum('r', arr, rows, cols);
	int *colsum = LineSum('c', arr, rows, cols);
	for (int i = 0; i < rows; i++)
	{
		arr[i][cols - 1] = (1 - rowsum[i]);
	}
	return 0;
}

int main(void)
{
	int rows, cols;
	printf("How many lines should the array have? ");
	scanf("%d", &rows);
	printf("\nHow many columns should the array have? ");
	scanf("%d", &cols);
	printf("\n\nThis is your array (sum of lines = 1):\n");
	
	int **arr = Dyn2dArray(rows, cols);
	magicSquare(arr, rows, cols);
    printArray(arr, rows, cols);
    
    for(int i = 0; i < rows; i++)
    {
        free(arr[i]);
    }
    free(arr);
    return 0;
}

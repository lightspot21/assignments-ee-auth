#include <stdio.h>
#include <math.h>

int bubblesort(int array[], int size)
{
	int i, j, temp;
	for (i = 1; i <= size; i++)
	{
		for (j = 0; j < (size - 1); j++)
		{
			if (array[j] > array[j + 1])
			{
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
				
		}
	}
}		


int percentiles(int array[], int size)
{
	int Q1, Q2, Q3 = 0;
	Q1 = array[(int) ceil(0.25 * size)];
	Q2 = array[(int) ceil(0.5 * size)];
	Q3 = array[(int) ceil(0.75 * size)];
	printf("\n\n25th percentile (Q1): %d\n", Q1);
	printf("50th percentile (Q2): %d\n", Q2);
	printf("75th percentile (Q3): %d\n", Q3);
	printf("=======================================\n");
}


int printIntArray(int array[], int size)
{
	int i = 0;
	for (i = 1; i <= (size - 1); i++)
	{
		printf("%d ", array[i]);
	}
}


int main(void)
{
	int i;
	int z[12] = {0, 11, 2, 4, 3, 1, 8, 5, 14, 10, 7, 6};
	printf("=======================================");
	printf("\nGiven array: \n");
	printIntArray(z, 12);
	bubblesort(z, 12);
	printf("\n\nSorted array: \n");
	printIntArray(z, 12);
	percentiles(z, 12);		
}


#include <stdio.h>
#include <math.h>


double f(double x)
{
	double value = 4 * log10(x - 1) + 3 * (x * x);
	return value;
}

double df(double x)
{
	double value = 4 / (log(10) * (x - 1)) + 6 * x;
	return value;
}

double newton(double guess, double limit)
{
	double approx = 0;
	int i = 1;
	while (fabs(f(guess)) > limit)
	{
		approx = guess - (f(guess)/df(guess));
		guess = approx;
		printf("Root approximation #%d: %lf\n", i, approx);
		i++;
	}
	printf("f's root is approximately equal to %lf.", approx);
}

int main(void)
{
	double start = 0;
	double limit = 0;
	printf("Newton's method for f(x) = 4log(x-1) + 3x^2\n");
	printf("Enter a starting value (must be greater than 1): \n");
	scanf("%lf", &start);
	printf("Enter a limit value (real number, smaller = better precision): \n");
	scanf("%lf", &limit);
	newton(start, limit);
}

# assignments-ee-auth
Various assignments made while studying Electrical and Computer engineering at AUTh.

* newton.c

This one finds a root of a function (namely f(x) = 4log(x-1)+3x^2) using the Newton method for root-finding.
* percentiles.c

A simple program, given an unsorted 1-dimensional array, it sorts it using bubblesort and prints the 25th, 50th and 75th percentile.

* eightqueens.c

Given a possible solution of the eight queens problem (more here: https://en.wikipedia.org/wiki/Eight_queens_puzzle), this program evaluates it via the use of sums, rather than tediously comparing the input with a preset solution list.

* magicsquare.c

Given the mxn dimensions of an array, the program prints an mxn array filled with numbers, so that the sum of every row must be equal to 1. (Solution of exam question, Jan 2018) 

